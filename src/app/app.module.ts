import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from './router/app-routing.module';
import {AppComponent} from './app.component';
import {HeaderComponent} from './container/header/header.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RecepesComponent} from './recepes/recepes.component';
import {RecepesItemComponent} from './recepes/recepes-item/recepes-item.component';
import {RecipesListComponent} from './recepes/recepes-list/recipes-list.component';
import {RecepieDetailComponent} from './recepes/recepie-detail/recepie-detail.component';
import {ShoppingListComponent} from './shopping-list/shopping-list.component';
import {ShoppingListAddComponent} from './shopping-list/shopping-list-add/shopping-list-add.component';
import {MDropdownDirective} from './directive/m-dropdown.directive';
import {RecipeService} from './service/recipe.service';
import {ShoppingService} from './service/shopping.service';
import {RecipeStartComponent} from './recepes/recipe-start/recipe-start.component';
import {RecipeEditComponent} from './recepes/recipe-edit/recipe-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    RecepesComponent,
    RecepesItemComponent,
    RecipesListComponent,
    RecepieDetailComponent,
    ShoppingListComponent,
    ShoppingListAddComponent,
    MDropdownDirective,
    RecipeStartComponent,
    RecipeEditComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgbModule.forRoot(),
    ReactiveFormsModule
  ],
  providers: [RecipeService, ShoppingService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
