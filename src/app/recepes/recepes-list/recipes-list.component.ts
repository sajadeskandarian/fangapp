import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Recipe} from '../../model/recipe';
import {RecipeService} from '../../service/recipe.service';

@Component({
  selector: 'app-recepes-list',
  templateUrl: './recipes-list.component.html',
  styleUrls: ['./recipes-list.component.css'],
})
export class RecipesListComponent implements OnInit {
  private recipes: Recipe[] = [];

  constructor(private recipeService: RecipeService) {
  }

  ngOnInit() {
    this.recipes = this.recipeService.getRecipes();
  }
}
