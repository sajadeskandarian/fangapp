import {Routes} from '@angular/router';
import {RecipeStartComponent} from './recipe-start/recipe-start.component';
import {RecepieDetailComponent} from './recepie-detail/recepie-detail.component';
import {RecipeEditComponent} from './recipe-edit/recipe-edit.component';

export const RECIPES_ROUTES: Routes = [
  {path: '', component: RecipeStartComponent},
  {path: 'new', component: RecipeEditComponent},
  {path: ':id', component: RecepieDetailComponent},
  {path: ':id/edit', component: RecipeEditComponent},
  {path: ':id/delete', component: RecipeEditComponent}
];
