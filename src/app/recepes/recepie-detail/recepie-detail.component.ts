import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Recipe} from '../../model/recipe';

import {ShoppingService} from '../../service/shopping.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {RecipeService} from '../../service/recipe.service';

@Component({
  selector: 'app-recepie-detail',
  templateUrl: './recepie-detail.component.html',
  styleUrls: ['./recepie-detail.component.css']
})
export class RecepieDetailComponent implements OnInit, OnDestroy {
  title = 'page title';
  private selected_recipe: Recipe;
  private routSubscription: Subscription;
  private id: number;

  constructor(private recipeService: RecipeService,
              private shoppingService: ShoppingService,
              private route: ActivatedRoute,
              private router: Router) {

  }

  ngOnInit() {
    this.routSubscription = this.route.params.subscribe((params: any) => {
      this.id = params['id'];
      this.selected_recipe = this.recipeService.getRecipe(this.id);
    });

  }


  addToList() {
    this.shoppingService.add(this.selected_recipe.ingredients);
  }

  ngOnDestroy(): void {
    this.routSubscription.unsubscribe();
  }

  onEdit() {
    this.router.navigate(['recipes', this.id, 'edit']);
  }

  onDelete() {
    this.router.navigate(['recipes', this.id, 'delete']);
  }
}
