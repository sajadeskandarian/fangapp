import {Component, Input} from '@angular/core';
import {Recipe} from '../../model/recipe';

@Component({
  selector: 'app-recepes-item',
  templateUrl: './recepes-item.component.html',
  styleUrls: ['./recepes-item.component.css']
})
export class RecepesItemComponent {
  @Input() recipe: Recipe;
  @Input() recipe_id: number;

}
