import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class Log {
  private text = 'private';

  constructor() {
  }

  setMessage(message: string) {
    this.text = message;
  }

  log() {
    console.log(this.text);
  }
}
