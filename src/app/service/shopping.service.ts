import {Injectable, Output} from '@angular/core';
import {Ingredient} from '../model/ingredient';

@Injectable()
export class ShoppingService {
  @Output() ingridents: Ingredient[] = [];

  constructor() {
  }

  add(ings: Ingredient[]) {
    Array.prototype.push.apply(this.ingridents, ings);
  }

  addItem(item: Ingredient) {
    this.ingridents.push(item);
  }

  getIngrident() {
    return this.ingridents;
  }

  editItem(oldIngridient: Ingredient, newIngrident: Ingredient) {
    this.ingridents[this.ingridents.indexOf(oldIngridient)] = newIngrident;
  }

  deleteItem(ingrident: Ingredient) {
    this.ingridents.splice(this.ingridents.indexOf(ingrident), 1);
  }
}
