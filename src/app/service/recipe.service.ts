import {Injectable} from '@angular/core';
import {Recipe} from '../model/recipe';
import {Ingredient} from '../model/ingredient';

@Injectable()
export class RecipeService {
  private s = 'بازی زولا یک بازی اکشن اول شخص آنلاین است که بازیکنان با ثبت نام و فعال سازی اکانت خود در بازی زولا می توانند آن را به صورت رایگان دانلود و بازی کنند، گیم پلی اصلی بازی زولا شبیه بازیهای رقابتی معروف مثل کانتر است که شما در قالب دو تیم در مقابل هم رقابت می کنید، تیمی که بتوانند بیشترین امتیاز یا تمام افراد تیم مقابل را شکست دهد برنده مسابقه خواهد شد، بازی زولا مخصوص سیستم عامل ویندوز است و به صورت دوره ای مسابقات کشوری برای این بازی به صورت آنلاین برگزار می شود. این بازی کاملا فارسی، به صورت رایگان خدمت شما ارایه می گردد.';
  private s2 = 'انیمیشن انتقام جویان: در جستجوی پلنگ سیاه (به انگلیسی: Marvel’s Avengers: Black Panther’s Quest 2018) سری جدیدی از مارول است که با محوریت شخصیت پلنگ سیاه، دوباره ابرقهرمانان دنیای سینمایی خودش را دورهم جمع می کند. در فصل پنجم انیمیشن Marvels Avengers، پلنگ سیاه با گروهی از دشمنان قدیم و جدید خودش روبرو می شود و مبارزات خودش را به منظور برقراری تعادل در سرزمین خودش انجام می دهد. حال پلنگ سیاه عضوی از گروه انتقام جویان شده است و باید وظایف جدید خودش را در این گروه انجام دهد.';
  recipes: Recipe[] = [
    new Recipe('بازی آنلاین زولا',
      this.s,
      'https://downloadha.com/shop/zolann-2.jpg', [
      new Ingredient('value 1', 1),
      new Ingredient('value 2', 2),
    ]),
    new Recipe('Marvels Avengers Black Panthers',
      this.s2,
      'https://img5.downloadha.com/hosein/files/2018/09/Avengers-Black-Panthers-Quest-2018-Cover-Small.jpg', [
      new Ingredient('value 3', 3),
      new Ingredient('value 4', 4),
    ]),
  ];

  constructor() {
  }

  getRecipes() {
    return this.recipes;
  }

  getRecipe(index: number) {
    return this.recipes[index];
  }
}
