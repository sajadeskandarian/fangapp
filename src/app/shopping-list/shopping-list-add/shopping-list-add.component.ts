import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {ShoppingService} from '../../service/shopping.service';
import {Ingredient} from '../../model/ingredient';

@Component({
  selector: 'app-shopping-list-add',
  templateUrl: './shopping-list-add.component.html',
  styleUrls: ['./shopping-list-add.component.css']
})
export class ShoppingListAddComponent implements OnInit, OnChanges {
  isAdd = true;
  @Input() item: Ingredient = null;
  @Output() cleared = new EventEmitter();

  constructor(private sls: ShoppingService) {
  }

  ngOnInit() {
  }


  onSubmit(ingredient: Ingredient) {
    const newIngredient = new Ingredient(ingredient.name, ingredient.amount);
    if (this.isAdd) {
      this.item = newIngredient;
      this.sls.addItem(this.item);
    } else {
      this.sls.editItem(this.item, newIngredient);
    }

  }

  onDelete() {
    this.sls.deleteItem(this.item);
    this.onClear();
  }

  onClear() {
    this.isAdd = true;
    this.cleared.emit(null);
  }

  ngOnChanges(changes) {
    if (changes.item.currentValue === null) {
      this.isAdd = true;
      this.item = {name: null, amount: null};
    } else {
      this.isAdd = false;
    }
  }
}
