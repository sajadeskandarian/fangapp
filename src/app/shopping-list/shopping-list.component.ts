import {Component, OnInit} from '@angular/core';
import {Ingredient} from '../model/ingredient';
import {ShoppingService} from '../service/shopping.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {
  ingredients: Ingredient[];
  selectedIngredient: Ingredient = null;

  constructor(private shoppingService: ShoppingService, private router: Router) {
  }

  ngOnInit() {
    this.ingredients = this.shoppingService.getIngrident();
  }

  goHome() {
    this.router.navigate(['']);
  }

  onSelect(ing: Ingredient) {
    this.selectedIngredient = ing;
  }

  onCleard() {
    this.selectedIngredient = null;
  }
}
