import {Directive, HostBinding, HostListener} from '@angular/core';

@Directive({
  selector: '[appMDropdown]',
  exportAs: 'appMDropdown'
})
export class MDropdownDirective {
  @HostBinding('class.open') isOpen = false;

  @HostListener('click') toggleopen() {
    this.isOpen = !this.isOpen;
  }
}
