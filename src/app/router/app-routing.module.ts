import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ShoppingListComponent} from '../shopping-list/shopping-list.component';
import {RecepesComponent} from '../recepes/recepes.component';
import {RECIPES_ROUTES} from '../recepes/recipes.routes';

const routes: Routes = [
  {path: '', redirectTo: '/recipes', pathMatch: 'full'},
  {path: 'recipes', component: RecepesComponent, children: RECIPES_ROUTES},
  {path: 'shopping-list', component: ShoppingListComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
